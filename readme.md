# Test technique Web-Atrio :

Ajouter une personne et la visualiser selon les règles décrites.

## Environnement de développement

### Pré-requis

 * PHP 7.4
 * Composer
 * Symfony CLI
 * Docker
 * Docker-compose
 * Symfony 5.5.3 & PHP 7.4.11

Pour vérifier les pré-requis (sauf Docker & Docker-composer) :
```bash
symfony check:requirements
```

### Lancer l'environnement de développement (Projet, Serveur, BDD & Mailer)

```bash
composer install
symfony serve -d
docker-compose up -d
```

### Lancer les tests

```bash
php bin/phpunit --testdox
```
### Test théorique

1) Retourner tous les éléments book
//book

2) Retourner tous les éléments title ayant comme parent un élément book avec un attribut type égal à roman
//book[@type='roman']/title

3) Retourner le nombre d'éléments book ayant un attribut type égal à bd
count(//book[@type='bd'])

4) Que renvoie la requête XPath suivante : /library/library/ancestor-or-self::library
Elle renvoie : 
<title>toto5</title>
<author>titi</author>
