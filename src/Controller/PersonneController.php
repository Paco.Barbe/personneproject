<?php

namespace App\Controller;

use App\Entity\Personne;
use App\Form\PersonneFormType;
use App\Repository\PersonneRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PersonneController extends AbstractController
{
    /**
     * @var Repository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * Constructeur
     *
     * @param \App\Repository\PersonneRepository $repository
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     */
    public function __construct(PersonneRepository $repository, EntityManagerInterface $entityManager)
    {
        $repository = $this->repository;
        $entityManager = $this->entityManager;
    }

    /**
     * @Route("/", name="accueil")
     */
    public function index(): Response
    {
        return $this->render('personne/index.html.twig', []);
    }


    /**
     * Ajout d'une personne via un formulaire
     *
     * @Route("/personne-add", name="personne_add")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function personneAdd(Request $request): Response
    {
        $personne = new Personne();
        $form = $this->createForm(PersonneFormType::class, $personne);
        $form->handleRequest($request);

        //Si le formulaire est soumis + valide
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($personne);
            $entityManager->flush();

            //Message si succès
            $this->addFlash('success', 'L\'utilisateur a été enregistré avec succès.');

            //Envoie vue accueil
            return $this->redirectToRoute('personnes_show');;
        }

        //Envoie du formulaire
        return $this->render('personne/personne-add.html.twig', [
            'personneForm' => $form->createView()
        ]);
    }


    /**
     * Liste des personnes
     * @Route("/personnes-show", name="personnes_show")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function personnesShow(PersonneRepository $personnes): Response
    {
        return $this->render('personne/personnes-show.html.twig', [
            'personnes' => $personnes->findBy(array(), array('nom' => 'ASC')),
        ]);
    }
}
