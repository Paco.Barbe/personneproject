<?php

namespace App\Tests;

use App\Entity\Personne;
use PHPUnit\Framework\TestCase;
use DateTime;

class PersonneUnitTest extends TestCase
{

    /**
     * Test simple de données rentrées
     *
     * @return void
     */
    public function testValueTure(): void
    {
        //Date de naissance
        $date_naissance = DateTime::createFromFormat('Y-m-d', '1997-05-03');

        //Personne 
        $personne = new Personne();

        $personne->setDateNaissance($date_naissance)
                 ->setNom('Barbe')
                 ->setPrenom('Paco');

        $this->assertTrue($personne->getDateNaissance() === $date_naissance);
        $this->assertTrue($personne->getNom() === 'Barbe');
        $this->assertTrue($personne->getPrenom() === 'Paco');
    }
}
